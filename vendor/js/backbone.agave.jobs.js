/**
 * Backbone Agave Jobs
 * Version 0.1
 *
 */
(function (window) {

"use strict";

var Backbone = window.Backbone;
var $ = window.$;
var _ = window._;

var Agave = Backbone.Agave;

var Jobs = Agave.Jobs = {};

Jobs.Job = Agave.Model.extend({
  urlRoot: '/apps-v1/job',
  sync: function(method, model, options) {
    if (method === "create") {
      // options.beforeSend = function(xhr) {
      //   xhr.setRequestHeader("jobName", model.get("jobName"));
      //   xhr.setRequestHeader("softwareName", model.get("softwareName"));
      // };
      options.emulateJSON = true;
      options.data = model.toJSON();
    }
    return Agave.sync(method, model, options);
  }
});

return Agave;
})(this);