(function() {
  var link = document.createElement('link');
  link.href = '//aci-dev.tacc.utexas.edu/mrhanlon/foundation-backbone-widgets/app/styles/index.css';
  link.rel = 'stylesheet';
  document.body.appendChild(link);

  var script = document.createElement('script');
  script.src = '//aci-dev.tacc.utexas.edu/mrhanlon/foundation-backbone-widgets/vendor/jam/require.js';
  script.dataset.main = '//aci-dev.tacc.utexas.edu/mrhanlon/foundation-backbone-widgets/app/config.js';
  document.body.appendChild(script);
})();
