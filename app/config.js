// Set the require.js configuration for your application.
require.config({

  // Initialize the application with the main application file and the JamJS
  // generated configuration file.
  deps: ["../vendor/jam/require.config", "main"],

  packages: [
  {
            "name": "backbone.agave",
            "location": "../vendor/js",
            "main": "backbone.agave.js"
        },
        {
            "name": "backbone.agave.io",
            "location": "../vendor/js",
            "main": "backbone.agave.io.js"
        },
        {
            "name": "backbone.agave.apps",
            "location": "../vendor/js",
            "main": "backbone.agave.apps.js"
        },
        {
            "name": "backbone.agave.jobs",
            "location": "../vendor/js",
            "main": "backbone.agave.jobs.js"
        },
        {
            "name": "fileSaver",
            "location": "../vendor/js",
            "main": "fileSaver.js"
        }
    ],

    shim: {
        "backbone.agave": {
            "deps": ["backbone"],
            "exports": "Backbone.Agave"
        },
        "backbone.agave.io": {
            "deps": ["backbone", "backbone.agave"],
            "exports": "Backbone.Agave"
        },
        "backbone.agave.jobs": {
            "deps": ["backbone", "backbone.agave", "backbone.agave.io"],
            "exports": "Backbone.Agave"
        },
        "backbone.agave.apps": {
            "deps": ["backbone", "backbone.agave", "backbone.agave.io", "backbone.agave.jobs"],
            "exports": "Backbone.Agave"
        }
    }

});
