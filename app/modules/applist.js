// Applist module
define([
  "hogan",
  "backbone"
],

// Map dependencies from above array.
function(Hogan, Backbone) {

  var Agave = Backbone.Agave;

  // Create a new module.
  var Applist = {};

  Applist.List = Backbone.View.extend({
    initialize: function() {
      this.collection.on('reset', this.render, this);
      this.collection.fetch({reset: true});
    },
    render: function() {
      var tmpl = Hogan.compile('<ul>{{#app}}<li><strong>{{name}} - {{version}}</strong> {{executionHost}}</li>{{/app}}</ul>');
      this.$el.html(tmpl.render({app: this.collection.toJSON() }));
    }
  });

  // Return the module for AMD compliance.
  return Applist;

});
