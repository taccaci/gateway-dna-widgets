// Form module
define(["hogan","backbone"],

function(Hogan, Backbone) {
  var Forms = {};
  Forms.Model = Backbone.Model.extend({
    defaults: {
      className: "field"
    }
  });

  Forms.Field = Backbone.View.extend({
    attributes: function() {
      return {
        "class": this.model.get("required") ? "field field-required" : "field"
      };
    },
    initialize: function() {
      this.tmpl = Hogan.compile(this.template);
    },
    render: function() {
      this.$el.html(this.tmpl.render(this.model.toJSON()));
    }
  });

  Forms.Text = Forms.Field.extend({
    template: '{{#label}}<label for="{{id}}">{{label}}</label>{{/label}}<input type="text" name="{{name}}" id="{{id}}" placeholder="{{prompt}}" value="{{defaultValue}}">{{#help}}<span class="help-block">{{help}}</span>{{/help}}'
  });

  Forms.Checkbox = Forms.Field.extend({
    template: '<label for="{{id}}" class="checkbox"><input type="checkbox" name="{{name}}" id="{{id}}" {{#defaultValue}}checked{{/defaultValue}}>{{label}}</label>{{#help}}<span class="help-block">{{help}}</span>{{/help}}'
  });

  Forms.Select = Forms.Field.extend({
    template: '{{#label}}<label for="{{id}}">{{label}}</label>{{/label}}<select name="{{name}}" id="{{id}}"><option>Choose one</option>{{#options}}<option value="{{option}}" {{#defaultValue}}selected{{/defaultValue}}>{{option}}</option>{{/options}}</select>{{#help}}<span class="help-block">{{help}}</span>{{/help}}'
  });

  Forms.Fieldset = Backbone.View.extend({
    template: '<legend>{{legend}}</legend>'
  });
  return Forms;
});