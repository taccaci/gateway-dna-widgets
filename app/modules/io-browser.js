// Io-browser module
define([
  // Application.
  "app"
],

// Map dependencies from above array.
function(app) {

  // Create a new module.
  var IOBrowser = app.module();

  // Default Model.
  IOBrowser.Model = Backbone.Model.extend({

  });

  // Default Collection.
  IOBrowser.Collection = Backbone.Collection.extend({
    model: IOBrowser.Model
  });

  // Default View.
  IOBrowser.Views.Layout = Backbone.Layout.extend({
    template: "io-browser"
  });

  // Return the module for AMD compliance.
  return IOBrowser;

});
