// Application module
define([
  "hogan",
  "backbone",
  "modules/form"
],

// Map dependencies from above array.
function(Hogan, Backbone, Form) {

  var Agave = Backbone.Agave;

  // Create a new module.
  var Application = {};

  Application.Form = Backbone.View.extend({
    initialize: function() {
      this.agave = new Backbone.Agave();
      this.model.on('change', this.render, this);
      if (this.agave.token().isValid()) {
        this.model.fetch();
      }
      this.delegateEvents();
    },
    events: {
      'click .btn-primary': 'submitForm',
      'click .btn-showForm': 'showForm'
    },
    render: function() {
      this.$el.empty();
      if (this.agave.token().isValid()) {
        this.renderForm();
      } else {
        this.renderPrompt();
      }
    },
    renderPrompt: function() {
      var el = $('<div>').appendTo(this.el),
        tmpl = Hogan.compile('<p>{{message}}</p><button class="btn btn-showForm">Get Form</button>');

      this.$el.html(tmpl.render({message: "You must have an iPlant token to use this application."}));
    },
    renderForm: function() {
      var sorter = function(o) { return o.id; },
        params = _.sortBy(this.model.get('parameters'), sorter),
        inputs = _.sortBy(this.model.get('inputs'), sorter),
        // outputs = _.sortBy(this.model.get('outputs'), sorter),
        form, fieldset, i, el;
      form = $('<form>').appendTo(this.el);
      if (params && params.length > 0) {
        el = $('<fieldset class="parameters">').appendTo(form);
        fieldset = new Form.Fieldset({
          el: el,
          model: new Form.Model({
            legend: "Parameters"
          })
        });
        fieldset.render();

        for (i = 0; i < params.length; i++) {
          var param = params[i];
          el = $("<div>").appendTo(fieldset.el);
          if (param.value.visible) {
            var model = new Form.Model({
              id: param.id,
              name: param.id,
              label: param.details.label,
              help: param.details.description === "" ? null : param.details.description,
              defaultValue: param.defaultValue,
              required: param.value.required
            });

            switch (param.value.type) {
            case "bool":
              model.set("defaultValue", param.defaultValue === "true");
              new Form.Checkbox({el: el, model: model}).render();
            break;

            case "string":
              var regexp = /^([^|]+[|])+[^|]+$/;
              if (regexp.test(param.value.validator)) {
                var optvals = param.value.validator.split("|");
                var options = [];
                for (var j = 0; j < optvals.length; j++) {
                  options.push({
                    option: optvals[j],
                    defaultValue: optvals[j] === param.defaultValue
                  });
                }
                model.set("options", options);
                new Form.Select({el: el, model: model}).render();
              } else {
                new Form.Text({el: el, model: model}).render();
              }
            break;

            default:
              new Form.Text({el: el, model: model}).render();
            }
          }
        }
      }

      if (inputs && inputs.length > 0) {
        el = $('<fieldset class="inputs">').appendTo(form);
        fieldset = new Form.Fieldset({
          el: el,
          model: new Form.Model({
            legend: "Input files"
          })
        });
        fieldset.render();

        for (i = 0; i < inputs.length; i++) {
          el = $("<div>").appendTo(fieldset.el);
          new Form.Text({
            el: el,
            model: new Form.Model({
              id: inputs[i].id,
              name: inputs[i].id,
              label: inputs[i].details.label,
              help: inputs[i].details.description === "" ? null : inputs[i].details.description,
              defaultValue: inputs[i].defaultValue,
              required: inputs[i].value.required
            })
          }).render();
        }
      }

      el = $('<fieldset class="metadata">').appendTo(form);
      fieldset = new Form.Fieldset({
        el: el,
        model: new Form.Model({legend: "Job Info"})
      });
      fieldset.render();

      el = $('<div>').appendTo(fieldset.el);
      new Form.Text({
        el: el,
        model: new Form.Model({
          id: "jobName",
          name: "jobName",
          label: "Job name",
          required: true,
          defaultValue: this.model.get("name") + "-" + new Date().getTime(),
          help: "Provide a name for this job"
        })
      }).render();

      el = $('<div>').appendTo(fieldset.el);
      new Form.Text({
        el: el,
        model: new Form.Model({
          id: "processorCount",
          name: "processorCount",
          label: "Requested Number of Processors for this Job",
          required: true,
          defaultValue: 1,
          help: "For non-parallel jobs, this should be 1."
        })
      }).render();

      el = $('<div>').appendTo(fieldset.el);
      new Form.Text({
        el: el,
        model: new Form.Model({
          id: "requestedTime",
          name: "requestedTime",
          label: "Requested wall time",
          required: true,
          defaultValue: "00:01:00",
          help: "Enter your requested Wall Time (hh:mm:ss)"
        })
      }).render();

      $('<button class="btn btn-primary">Submit Form</button>').appendTo(form);
    },
    showForm: function(e) {
      var that = this;
      this.agave.token({
        username: window.AGAVE_USERNAME,
        token: window.AGAVE_TOKEN
      });
      this.agave.token().fetch({
        success: function() {
          that.model.fetch();
        }
      });
    },
    submitForm: function(e) {
      e.preventDefault();
      var form = $(e.currentTarget).closest('form');
      var params = $('.parameters', form).serializeArray(),
        inputs = $('.inputs', form).serializeArray(),
        metadata = $('.metadata', form).serializeArray(),
        job = {},
        i;
      if (params.length > 0) {
        for (i = 0; i < params.length; i++) {
          if (params[i].value !== "") {
            job[params[i].name] = params[i].value;
          }
        }
      }
      if (inputs.length > 0) {
        for (i = 0; i < inputs.length; i++) {
          if (inputs[i].value !== "") {
            job[inputs[i].name] = inputs[i].value;
          }
        }
      }
      if (metadata.length > 0) {
        for (i = 0; i < metadata.length; i++) {
          if (metadata[i].value !== "") {
            job[metadata[i].name] = metadata[i].value;
          }
        }
      }
      job.softwareName = this.model.id;
      // todo?
      job.archive = true;
      job.archivePath = "/" + this.agave.token().get("username") + "/" + job.jobName;
      var appJob = new Backbone.Agave.Jobs.Job();
      appJob.save(job, {
        success: function() {
          alert('Your job was submitted successfully! Job ID: ' + appJob.id);
        }
      });
      return false;
    }
  });

  // Return the module for AMD compliance.
  return Application;

});
