// Droplet module
define([
  "hogan",
  "backbone"
],

// Map dependencies from above array.
function(Hogan, Backbone) {

  // Create a new module.
  var Droplet = {};

  // Default View.
  Droplet.Main = Backbone.View.extend({
    template: "droplet",
    initialize: function() {
      this.delegateEvents();
    },
    events: {
      'dragover .agave-io-droplet': function(e) {
        e.stopPropagation();
        e.preventDefault();
        e.originalEvent.dataTransfer.dropEffect = 'copy';
      },
      'drop .agave-io-droplet': function(e) {
        e.stopPropagation();
        e.preventDefault();
        var formData = new FormData(),
          files = e.originalEvent.dataTransfer.files;
        for (var i = 0; i < files.length; i++) {
          formData.append('fileToUpload', files[i]);
        }

        var agave = new Backbone.Agave();
        console.log(agave.token().toJSON());
        var xhr = new XMLHttpRequest();
        var url = Backbone.Agave.agaveApiRoot + '/io-v1/io/' + agave.token().get('username');
        xhr.open('POST', url , true);
        xhr.setRequestHeader('Authorization', 'Basic ' + agave.token().getBase64());
        var that = this;
        xhr.onload = function(e) {
          if (this.status === 200) {
            alert('File uploaded to: ' + JSON.parse(this.response).result.path);
          }
        };
        xhr.send(formData);
      }
    },
    render: function() {
      var tmpl = Hogan.compile('<div class="agave-io-droplet"><p>{{message}}</p></div>');
      this.$el.html(tmpl.render({message:"Drop files here to upload to iPlant!"}));
    }
  });

  // Return the module for AMD compliance.
  return Droplet;

});
