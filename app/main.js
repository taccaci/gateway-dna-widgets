require([
  // Application.
  "backbone",
  "modules/droplet",
  "modules/applist",
  "modules/application",

  // plugins
  "backbone.agave",
  "backbone.agave.io",
  "backbone.agave.jobs",
  "backbone.agave.apps"
],

function(Backbone, Droplet, Applist, Application) {
  $('.agave-widget').each(function(i,o) {
    switch(o.dataset.widget) {
      case "droplet":
        new Droplet.Main({el: o}).render();
      break;

      case "applist":
        new Applist.List({
          el: o,
          collection: new Backbone.Agave.Apps.PublicApplications()
        }).render();
      break;

      case "application":
        new Application.Form({
          el: o,
          model: new Backbone.Agave.Apps.Application({id: o.dataset['applicationId']})
        }).render();
      break;
    }
  });

});
