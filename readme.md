# Gateway DNA Widgets

Part of the [Agave API Toolkit](http://agaveapi.co/gateway-dna/).

The Backbone Widgets are a set of simple widgets that can be added to any
website the same way a twitter badge, or Facebook "Like" button is added. Simply
add 2 lines of HTML to your page and the widget will show up natively in your
page as HTML you can style using CSS.
